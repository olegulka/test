import kong.unirest.HttpResponse;
import kong.unirest.JsonNode;
import kong.unirest.Unirest;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import java.util.HashMap;
import java.util.Map;

import static org.testng.Assert.*;

public class YandexTest {

    private double LAT_VALUE = 55.75396;
    private double LON_VALUE = 37.620393;
    private int LIMIT_VALUE = 2;

    @BeforeTest
    private HttpResponse<JsonNode> getJSON() {

        String yandexApiUrl = "https://api.weather.yandex.ru/v1/forecast/";
        String apiKey = "4e79d515-1bb9-4137-bcab-8dc592ae9f57";

        final Map<String, Object> PARAMS = new HashMap<String, Object>();
        PARAMS.put("lat", LAT_VALUE);
        PARAMS.put("lon", LON_VALUE);
        PARAMS.put("limit", LIMIT_VALUE);

        Unirest.config()
                .setDefaultHeader("X-Yandex-API-Key", apiKey);

        HttpResponse<JsonNode> json = Unirest.get(yandexApiUrl)
                .queryString(PARAMS)
                .asJson();
        return json;
    }

    private HttpResponse<JsonNode> response = getJSON();

    @Test(groups = "info")
    void latTest() {
        double lat = response.getBody().getObject().getJSONObject("info").getDouble("lat");
        assertEquals(lat, LAT_VALUE);
    }

    @Test(groups = "info")
    void lonTest() {
        double lon = response.getBody().getObject().getJSONObject("info").getDouble("lon");
        assertEquals(lon, LON_VALUE);
    }

    @Test(groups = "tzinfo")
    void offsetTest() {
        int OFFSET_VALUE = 10800;
        int offset = response.getBody().getObject().getJSONObject("info").getJSONObject("tzinfo").getInt("offset");
        assertEquals(offset, OFFSET_VALUE);
    }

    @Test(groups = "tzinfo")
    void nameTest() {
        String NAME_VALUE = "Europe/Moscow";
        String name = response.getBody().getObject().getJSONObject("info").getJSONObject("tzinfo").getString("name");
        assertEquals(name, NAME_VALUE);
    }

    @Test(groups = "tzinfo")
    void abbrTest() {
        String ABBR_VALUE = "MSK";
        String abbr = response.getBody().getObject().getJSONObject("info").getJSONObject("tzinfo").getString("abbr");
        assertEquals(abbr, ABBR_VALUE);
    }

    @Test(groups = "tzinfo")
    void dstTest() {
        boolean dst = response.getBody().getObject().getJSONObject("info").getJSONObject("tzinfo").getBoolean("dst");
        assertFalse(dst, "Sign of summer time");
    }

    @Test(groups = "info")
    void urlTest() {
        String url = response.getBody().getObject().getJSONObject("info").getString("url");
        String correctUrl = "https://yandex.ru/pogoda/?lat=" + LAT_VALUE + "&lon=" + LON_VALUE;
        assertEquals(url, correctUrl);
    }

    @Test
    void sumOfDaysTest() {
        int sumOfDays = response.getBody().getObject().getJSONArray("forecasts").length();
        assertEquals(sumOfDays, LIMIT_VALUE);
    }

    @Test(groups = "fact")
    void seasonTest() {
        String season = response.getBody().getObject().getJSONObject("fact").getString("season");
        String date = response.getBody().getObject().getString("now_dt");
        int month = Integer.parseInt(date.substring(5, 7));

        switch (month) {
            case 12:
            case 1:
            case 2:
                assertEquals(season, "winter");
                break;
            case 3:
            case 4:
            case 5:
                assertEquals(season, "spring");
                break;
            case 6:
            case 7:
            case 8:
                assertEquals(season, "summer");
                break;
            case 9:
            case 10:
            case 11:
                assertEquals(season, "autumn");
                break;
        }
    }

    @Test(groups = "second day")
    void secondMoonCodeTest() {
        int moon_code = response.getBody().getObject().getJSONArray("forecasts").getJSONObject(1).getInt("moon_code");
        String moon_text = response.getBody().getObject().getJSONArray("forecasts").getJSONObject(1).getString("moon_text");

        switch (moon_code) {
            case 0:
                assertEquals(moon_text, "full-moon");
                break;
            case 1:
            case 2:
            case 3:
            case 5:
            case 6:
            case 7:
                assertEquals(moon_text, "decreasing-moon");
                break;
            case 4:
                assertEquals(moon_text, "last-quarter");
                break;
            case 8:
                assertEquals(moon_text, "new-moon");
                break;
            case 9:
            case 10:
            case 11:
            case 13:
            case 14:
            case 15:
                assertEquals(moon_text, "growing-moon");
                break;
            case 12:
                assertEquals(moon_text, "first-quarter");
                break;
            default:
                Assert.fail("moon code incorrect");
                break;
        }
    }

}
